from django.http import HttpResponse
from django.views import View


class MyView(View):
    """A simple view"""
    def get(self, request):
        """Simple docstring to demonstrate method description
        This is main page
        """
        return HttpResponse('result')
