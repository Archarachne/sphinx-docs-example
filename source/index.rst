.. sphinx-docs documentation master file, created by
   sphinx-quickstart on Sat Feb  8 20:54:56 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx-docs's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/index
   modules/views

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
