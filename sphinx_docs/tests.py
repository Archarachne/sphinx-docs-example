import unittest
import logging


class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        print("upper")
        logging.info("logging upper")
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        print("isupper")
        logging.info("logging isupper")
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        print("split")
        logging.info("logging split")
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)


if __name__ == '__main__':
    unittest.main()
