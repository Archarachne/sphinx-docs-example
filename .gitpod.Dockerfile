FROM archarachne/python-ubuntu:3.8

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

RUN apt-get update -y
RUN apt-get install -y gdal-bin libgdal-dev

RUN mkdir /app
WORKDIR /app

ADD pyproject.toml poetry.lock /app/
RUN poetry config virtualenvs.create false --local && poetry install --no-dev